<!DOCTYPE html>
<html>
<body>
<?php
/* Multidimensional 2D array for 4 books and each book having a different array containing book name, cost and type. */
$books = array(
array("Fiction ", "Action and Adventure ", 800),
array("Fiction ", "Anthology ", 1000),
array("Non- Fiction ", "Biography ", 600),
array("Non- Fiction ", "Cook Book ", 900)
);
/* Accessing of a 2D array with the row_number and column_number */
for ($row_num = 0; $row_num < 4; $row_num++) {
echo "<p>Book number is  $row_num</p>";
for ($col_num = 0; $col_num < 3; $col_num++) {
// Accessing a particular element in a 2D array
echo $books[$row_num][$col_num];
}
echo "<br>";
}
?>