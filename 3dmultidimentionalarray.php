<!DOCTYPE html>
<html>
<body>
<?php
$Employee = array(array(array("name", "company", "year"),
array("id","skills","profile"),
array("city","state","country")
),
/* array to store the name, company and year of employee*/
array(array("jiya", "Infosys", 2016),
array("ram", "ola", 2017)
),
/* array to store the id, skills and profile of employees */
array(array("E101", "PHP", "developer"),
array("E103", "mysql", "DBA")
),
/* array to store the city, state and country of employees */
array(array("Bangalore", "Karnataka", "India"),
array("San Francisco", "California", "USA")
)
);
?>
<?php
echo "<ul>";
for ( $outermost = 0; $outermost < 3; $outermost++ )
{
echo "<li>The outermost number $outermost";
echo "<ul>";
for ( $row_num = 0; $row_num < 2; $row_num++ )
{
echo "<li> Now displaying the row number $row_num";
echo "<ul>";
for ( $col_num = 0; $col_num < 3; $col_num++ )
{
// accessing the array elements in a 3D array
echo "<li>".$Employee[$outermost][$row_num][$col_num]."</li>";
}
echo "</ul>";
echo "</li>";
}
echo "</ul>";
echo "</li>";
}
echo "</ul>";
?>
</body>
</html>